<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


/**
 * Class types
 * @package b4k\phpTools
 */
class types
	{

	/**
	 * @param $str
	 *
	 * @return bool
	 */
	public static function stringIsNotNullOrEmpty($str) {
			if (isset($str)) {
				if(trim($str) != "") {
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

	public static function objectIsNotNullOrEmpty($obj, $property) {
		if (isset($obj)) {
			if(isset($obj->{$property})) {
				return true;
			}
			else
			{
				return false;
			}

		}
		else
		{
			return false;
		}
	}

	}
