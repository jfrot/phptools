<?php
/*
 * Copyright (c) 2020-2020. 28/10/2020 19:04. Johann Frot - B4K
 */

namespace b4k\phpTools;

	class arrayAndObject
	{
		public static function findItembyValue($arr, $valueKey, $value) {
			$item = null;
			foreach($arr as $arrayItem) {
				if ($arrayItem->{$valueKey} == $value) {
					$item = $arrayItem;
					break;
				}
			}
			return $item;
		}

		public static function findValuebyKey($arr, $keyName) {
			$value = null;
			foreach($arr as $elem) {
				if ($elem->name == $keyName) {
					$value = $elem->value;
					break;
				}
			}
			return $value;
		}

        public static function findArrayItemByValue($arr, $valueKey, $value) {
            $item = null;
            foreach($arr as $arrayItem) {
                if ($arrayItem[$valueKey] == $value) {
                    $item = $arrayItem;
                    break;
                }
            }
            return $item;
        }

        public static function convertObjectClass($object, $final_class) {
            return unserialize(sprintf(
                'O:%d:"%s"%s',
                strlen($final_class),
                $final_class,
                strstr(strstr(serialize($object), '"'), ':')
            ));
        }
	}
