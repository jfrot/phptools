<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


/**
 * Class url
 * @package b4k\phpTools\url
 */
class url
{

	public $host;
	public $root;
	public $site;

	/**
	 * @return string
	 */
	public static function getUrl() {
		$url  = $_SERVER["REQUEST_SCHEME"].'://'.$_SERVER["SERVER_NAME"];
		$url .= $_SERVER["REQUEST_URI"];
		return $url;
	}

	/**
	 * @param $context
	 *
	 * @return url
	 */
	public static function getUrlParts($context) {

		$url = new url();

		if ($context == "LOCAL") {
			$urlParts = explode("/", $_SERVER["REQUEST_URI"]);
			$url->host = "http://localhost";
			$url->root = "/" . $urlParts[1];
		}
		else
		{
			$url->host = trim(self::getUrl(), "/");
			$url->root = "";
		}

		$url->site = $url->host . $url->root;

		return $url;
	}

}
