<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


/**
 * Class math
 * @package b4k\phpTools
 */
class math
{

	/**
	 * @param $n
	 *
	 * @return array
	 */
	public static function fibonacci($n)
	{
		$sequence = [0, 1];

		for ($i = 2; $i < $n; $i++) {
			$sequence[$i] = $sequence[$i-1] + $sequence[$i-2];
		}

		return $sequence;
	}

}
