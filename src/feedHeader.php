<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


	class feedHeader
	{
		public $title;
		public $link;
		public $description;
		public $language;
		public $atomLink;
		public $faviconLink;
	}
