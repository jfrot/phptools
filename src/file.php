<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


/**
 * Class file
 * @package b4k\phpTools
 */
class file
{

	/**
	 * @param string $dirOrFile
	 *
	 * @return bool
	 */
	public static function checkDirectoryOrFile(string $dirOrFile) {
		return file_exists($dirOrFile);
	}

	/**
	 * @param string $dir
	 *
	 * @return bool
	 */
	public static function checkMakeDirectory(string $dir) {

		if (self::checkDirectoryOrFile($dir))
		{
			return true;
		}
		else
		{
			return mkdir($dir, 0777, true);
		}
	}

	/**
	 * @param $dir
	 */
	public static function removeDirAndContent($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
				if ($object != "." && $object != "..") {
					if (is_dir($dir."/".$object))
						file::removeDirAndContent($dir."/".$object);
					else
						unlink($dir."/".$object);
				}
			}
			rmdir($dir);
		}
	}

	public static function fileSize_human($filename) {
		$nbBytes = filesize($filename);
		$bytes = floatval($nbBytes);

		$arBytes = array(
			0 => array(
				"UNIT" => "TB",
				"VALUE" => pow(1024, 4)
			),
			1 => array(
				"UNIT" => "GB",
				"VALUE" => pow(1024, 3)
			),
			2 => array(
				"UNIT" => "MB",
				"VALUE" => pow(1024, 2)
			),
			3 => array(
				"UNIT" => "KB",
				"VALUE" => 1024
			),
			4 => array(
				"UNIT" => "B",
				"VALUE" => 1
			),
		);

		foreach($arBytes as $arItem)
		{
			if($bytes >= $arItem["VALUE"])
			{
				$result = $bytes / $arItem["VALUE"];
				$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
				break;
			}
		}
		return $result;
	}

	/**
	 * @param $filename
	 *
	 * @return string
	 */
	public static function extensionFichier($filename) {
		$ext = explode('.', basename($filename));
		return strtolower(array_pop($ext));
	}

    public static function removeExtensionFichier($filename) {
        $fileRootName = explode('.', basename($filename))[0];
        return strtolower($fileRootName);
    }

	/**
	 * @param $filename
	 *
	 * @return string
	 */
	public static function filenameRoot($filename) {
		$ext = explode('.', basename($filename));
		return rtrim(basename($filename), "." . array_pop($ext));
	}

	/**
	 * @param $directory
	 */
	public static function recursiveRemoveDirectory($directory)
	{
		foreach(glob("{$directory}/*") as $file)
		{
			if(is_dir($file)) {
				self::recursiveRemoveDirectory($file);
			} else {
				unlink($file);
			}
		}
		rmdir($directory);
	}

	/**
	 * @param $url
	 *
	 * @return bool
	 */
	public static function checkRemoteFile($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		// don't download content
		curl_setopt($ch, CURLOPT_NOBODY, 1);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		if(curl_exec($ch)!==FALSE)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    public static function does_url_exists($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

	/**
	 * @param $path
	 *
	 * @return bool|string
	 */
	public static function file_content($path)
	{
		$fp = fopen ($path, "r");
		$content = fread($fp, filesize($path));
		fclose ($fp);
		return $content;
	}

	//
// load a tab seperated text file as array
//
	public static function load_tabbed_file($filepath, $load_keys=false){
		$array = array();

		if (!file_exists($filepath)){ return $array; }
		$content = file($filepath);

		for ($x=0; $x < count($content); $x++){
			if (trim($content[$x]) != ''){
				$line = explode("\t", trim($content[$x]));
				if ($load_keys){
					$key = array_shift($line);
					$array[$key] = $line;
				}
				else { $array[] = $line; }
			}
		}
		return $array;
	}

}
