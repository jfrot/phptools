<?php
/*
 * Copyright (c) 2020-2021. 05/01/2021 16:08. Johann Frot - B4K
 */

namespace b4k\phpTools;


use Exception;
use Phits\Fits\FitsThumbnail;

/**
 * Class image
 * @package b4k\phpTools
 */
class image
	{

	/**
	 * @param $ext
	 * @param $newFilenameRoot
	 * @param $upload_dir
	 * @param $original_image_path
	 * @param $maxWidth
	 * @param $maxHeight
	 * @param $suffix
	 */
	public static function imageResize($ext, $newFilenameRoot, $upload_dir, $original_image_path, $maxWidth, $maxHeight, $suffix) {

			//Récupération de la photo
			if ($ext=='gif') {
				$img = imagecreatefromgif($original_image_path);
			}
			elseif ($ext=='png') {
				$img = imagecreatefrompng($original_image_path);
			}
			else
			{
				$img = imagecreatefromjpeg($original_image_path);
			}

			$size = getimagesize($original_image_path);//Taille de la photo
			$width = $size[0];//largeur
			$height = $size[1];//hauteur

			//nouvelles dimensions de la photo
			$ratio = $width / $height;

			if ($maxWidth == -1 && $maxHeight != -1)
			{
				$width = $maxHeight * $ratio;
				$height = $maxHeight;
			}
			else if ($maxWidth != -1 && $maxHeight == -1)
			{
				$width = $maxWidth;
				$height = $maxWidth / $ratio;
			}
			else
			{
				if ($ratio > 1) {
					$width = $maxWidth;
					$height = $maxWidth / $ratio;
				} elseif ($ratio < 1) {
					$width = $maxHeight * $ratio;
					$height = $maxHeight;
				} else //ratio = 1
				{
					$width = $maxWidth;
					$height = $maxHeight;
				}
			}

			$resizedImage = imagecreatetruecolor($width,$height); //Création de la nouvelle image (vide)
			$whiteBackground = imagecolorallocate ( $resizedImage, 255, 255, 255 );//Définition du fond blanc
			imagefill ( $resizedImage, 0, 0, $whiteBackground );//Remplissage du fond blanc
			imagecopyresampled($resizedImage,$img,0,0,0,0,$width,$height,$size[0],$size[1]);//copie, redimensionnement et rééchantillonage de l'image d'origine vers l'image finale
			imageinterlace ( $resizedImage, 1 );//Entrelacement pour créer un jpg progressif

			$dest_nouv = $upload_dir. DIRECTORY_SEPARATOR. $newFilenameRoot ."-" . $suffix . ".jpg";
			imagejpeg($resizedImage, $dest_nouv);//enregistrement du fichier image

			imagedestroy($img);
			imagedestroy($resizedImage);
		}

	/**
	 * @param $img_orig
	 * @param $filename
	 * @param $new_filename
	 * @param $upload_dir
	 * @param $maxWidth
	 * @param $maxHeight
	 * @param $suffix
	 *
	 * @return bool
	 */
	public static function redimensionner($img_orig, $filename, $new_filename, $upload_dir, $maxWidth, $maxHeight, $suffix) {

			try {

				$ext = file::extensionFichier($filename);

				//Récupération de la photo
				if ($ext=='gif') {
					$img = imagecreatefromgif($img_orig);
				}
				elseif ($ext=='png') {
					$img = imagecreatefrompng($img_orig);
				}
				else
				{
					$img = imagecreatefromjpeg($img_orig);
				}

				$width0 = imagesx($img);
				$height0 = imagesy($img);

				//nouvelles dimensions de la photo
				$ratio = $width0 / $height0;

				if ($maxWidth == -1 && $maxHeight != -1)
				{
					$width = $maxHeight * $ratio;
					$height = $maxHeight;
				}
				else if ($maxWidth != -1 && $maxHeight == -1)
				{
					$width = $maxWidth;
					$height = $maxWidth / $ratio;
				}
				else
				{
					if ($ratio > 1) {
						$width = $maxWidth;
						$height = $maxWidth / $ratio;
					} elseif ($ratio < 1) {
						$width = $maxHeight * $ratio;
						$height = $maxHeight;
					} else //ratio = 1
					{
						$width = $maxWidth;
						$height = $maxHeight;
					}
				}

				$resizedImage = imagecreatetruecolor($width,$height); //Création de la nouvelle image (vide)
				$whiteBackground = imagecolorallocate ( $resizedImage, 255, 255, 255 );//Définition du fond blanc
				imagefill ( $resizedImage, 0, 0, $whiteBackground );//Remplissage du fond blanc
				imagecopyresampled($resizedImage,$img,0,0,0,0,$width,$height,$width0,$height0);//copie, redimensionnement et rééchantillonage de l'image d'origine vers l'image finale
				imageinterlace ( $resizedImage, 1 );//Entrelacement pour créer un jpg progressif
				//$new_filename = $new_filename .".jpg";//nom du fichier

				$dest_nouv = $upload_dir. DIRECTORY_SEPARATOR. $new_filename ."-" . $suffix . ".jpg";
				imagejpeg($resizedImage, $dest_nouv);//enregistrement du fichier image

				imagedestroy($img);
				imagedestroy($resizedImage);

				return $new_filename;
			}
			catch (Exception $e) {
				return false;
			}
		}

	/**
	 * @param $nomFichier
	 *
	 * @return bool
	 */
	public static function isImage($nomFichier) {
			$nomFichierParts = explode('.', $nomFichier);
			$extension =  strtolower(end($nomFichierParts));
			if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png')
			{
				return true;
			}
			else
			{
				return false;
			}
		}


	/**
	 * @param $image_url
	 *
	 * @return bool
	 */
	public static function getimagesize($image_url){
			$handle = fopen ($image_url, "rb");
			$contents = "";
			if ($handle) {
				do {
					$data = fread($handle, 8192);
					if (strlen($data) == 0) {
						break;
					}
					$contents .= $data;
				} while(true);
			} else { return false; }
			fclose ($handle);

			$im = imagecreatefromstring($contents);
			if (!$im) { return false; }
			$gis[0] = imagesx($im);
			$gis[1] = imagesy($im);
			imagedestroy($im);
			return $gis;
		}

		public static function createCloudinaryUrl($cloudinary_cloud_name, $img_url, $cloudinary_transformations): string
        {
            return "https://res.cloudinary.com/"
                . $cloudinary_cloud_name
                . "/image/fetch/" . $cloudinary_transformations
                . "/" . $img_url;
        }

	public static function createCloudinaryUploadUrl($cloudinary_cloud_name, $img_path, $cloudinary_transformations): string
	{
		return "https://res.cloudinary.com/"
			. $cloudinary_cloud_name
			. "/image/upload/" . $cloudinary_transformations
			. "/" . $img_path;
	}
	}
