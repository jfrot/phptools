<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


/**
 * Class alertType
 * @package b4k\phpTools
 */
class alertType
{

	const muted = "muted";
	const primary = "primary";
	const success = "success";
	const info = "info";
	const warning = "warning";
	const danger = "danger";

}
