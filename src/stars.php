<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


/**
 * Class url
 * @package b4k\phpTools\stars
 */
class stars
{

	public static function displayStars($nb_stars, $html_star_code): string
	{
		$return_string = '';

		if ($nb_stars == 1) {
			$return_string = $html_star_code;
		}

		if ($nb_stars == 2) {
			$return_string = $html_star_code . $html_star_code;
		}

		if ($nb_stars == 3) {
			$return_string = $html_star_code . $html_star_code . $html_star_code;
		}

		if ($nb_stars == 4) {
			$return_string = $html_star_code . $html_star_code . $html_star_code . $html_star_code;
		}

		if ($nb_stars == 5) {
			$return_string = $html_star_code . $html_star_code . $html_star_code . $html_star_code . $html_star_code;
		}

		return $return_string;
	}

}
