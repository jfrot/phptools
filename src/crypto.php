<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;

class crypto
{
	public static function encrypt($strToEncrypt, $key) {
        return openssl_encrypt($strToEncrypt, "AES-128-ECB", $key);
	}

    public static function decrypt($strToDecrypt, $key) {
        return openssl_decrypt($strToDecrypt, "AES-128-ECB", $key);
    }
}
