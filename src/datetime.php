<?php
/*
 * Copyright (c) 2020-2020. 03/12/2020 10:31. Johann Frot - B4K
 */

namespace b4k\phpTools;


/**
 * Class datetime
 * @package b4k\phpTools
 */
class datetime
{

	/**
	 * @param $date
	 * @param $format
	 *
	 * @return false|string
	 */
	public static function getLanguageFormattedDate($date, $format) {
		return date($format, strtotime($date));
	}

    public static function age($date) {
        $age = date('Y') - $date;
        if (date('md') < date('md', strtotime($date))) {
            return $age - 1;
        }
        return $age;
    }

}
