<?php
/*
 * Copyright (c) 2017-2020. 28/10/2020 10:57. Johann Frot - Optima Lab
 */

namespace b4k\phpTools;


	class feedItem
	{
		public $title;
		public $link;
		public $description;
        public $content;
		public $pubDate;
		public $guid;
		public $image_url;
		public $image_type;
		public $image_length;
	}