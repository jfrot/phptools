<?php
/*
 * Copyright (c) 2020-2020. 13/11/2020 09:49. Johann Frot - B4K
 */

namespace b4k\phpTools;


/**
 * Class error
 * @package b4k\phpTools
 */
class error
{
	public $code;
	public $message;
	public $alertType;

	/**
	 * @param $class
	 *
	 * @return bool
	 */
	public static function checkError($class) {
//		if (false) $class->error = new error();
		if ($class->error->code !== error::success()->code) {
			info::displayInfo("Bootstrap", $class->error->message, $class->error->alertType);
			return true;
		}
		return false;
	}

	/**
	 * @param string $filePath
	 *
	 * @return error
	 */
	public static function success() {
		$message = "";
		$alertType = alertType::success;
		return self::makeError("0", $message, $alertType);
	}

	/**
	 * @param string $filePath
	 *
	 * @return error
	 */
	public static function file_not_exists(string $filePath) {
		$message = "Le fichier <strong>[FILE_PATH]</strong> n'existe pas";
		$alertType = alertType::danger;
		return self::makeError("100", $message, $alertType, "[FILE_PATH]", $filePath);
	}

	/**
	 * @param string $folderPath
	 *
	 * @return error
	 */
	public static function folder_not_exists(string $folderPath) {
		$message = "Le dossier <strong>[FOLDER_PATH]</strong> n'existe pas";
		$alertType = alertType::danger;
		return self::makeError("200", $message, $alertType, "[FOLDER_PATH]", $folderPath);
	}

	/**
	 * @param string $folderPath
	 *
	 * @return error
	 */
	public static function folder_creation_failed(string $folderPath) {
		$message = "La création du dossier <strong>[FOLDER_PATH]</strong> a échoué";
		$alertType = alertType::danger;
		return self::makeError("300", $message, $alertType, "[FOLDER_PATH]", $folderPath);
	}

    /**
     * @param $code
     * @param $message
     * @param $alertType
     * @param null $stringToReplace
     * @param null $replacement
     *
     * @return error
     */
	private static function makeError($code, $message, $alertType, $stringToReplace = null, $replacement = null) {

		$error = new error();
		$error->code = $code;

		if (isset($stringToReplace) && isset($replacement)) {
			$error->message = str_replace($stringToReplace, $replacement, $message);
		}
		else
		{
			$error->message = $message;
		}

		$error->alertType = $alertType;

		return $error;
	}
}
