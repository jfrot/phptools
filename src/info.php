<?php
/*
 * Copyright (c) 2020-2020. 28/10/2020 19:04. Johann Frot - B4K
 */

namespace b4k\phpTools;


class info
{

	public static function displayInfo(string $CSS_STYLE_TYPE, string $message, string $messageType) {
		$html = $message;

		if ($CSS_STYLE_TYPE === "Bootstrap") {
			$html = '<div class="alert alert-' . $messageType . '" role="alert" style="margin-top:20px;">' . $message . '</div>';
		}

		echo $html;
	}

}
