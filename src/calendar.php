<?php
/*
 * Copyright (c) 2020-2020. 29/12/2020 16:37. Johann Frot - B4K
 */

namespace b4k\phpTools;


class calendar
{

	public static function formatDate($date) {
		$date = date_parse($date);
		return self::premierJour($date['day']) . ' ' . self::nomMois($date['month']) . ' ' . $date['year'] ;
	}

	public static function formatDate2($date) {
		$date = date_parse($date);
		return self::premierJour2($date['day']) . ' ' . self::nomMois2($date['month']) . ' ' . $date['year'] ;
	}

	public static function formatMoisAnnee($mois, $annee) {
		return self::nomMois($mois) . ' ' . $annee ;
	}

	public static function formatMoisAnnee2($mois, $annee) {
		return self::nomMois2($mois) . ' ' . $annee ;
	}

	public static function formatDateDDMMYYYY($date) {
		$date = date_parse($date);
		return $date['day'] . '/' . $date['month'] . '/' . $date['year'] ;
	}

	public static function formatDateMMDDYYYY($date) {
		$date = date_parse($date);
		return $date['month'] . '/' . $date['day'] . '/' . $date['year'] ;
	}

	public static function formatDateYYYYMMDD($date) {
		$date = date_parse($date);
		return $date['year'] . '/' . $date['month'] . '/' . $date['day'] ;
	}

	public static function formatPrix($prix, $symbole) {
		return $prix . "&nbsp;" . $symbole;
	}

	public static function sqlBetweenMois($champDate, $date) {
		$date = date_parse($date);

		$annee1 = $date['year'];
		$mois1 = $date['month'];

		$annee2 = ($mois1<12 ? $annee1 : ($annee1 + 1));
		$mois2 = ($mois1<12 ? ($mois1 + 1) : '01');
		//return "BETWEEN '" . $annee1."-".$mois1."-01' AND '" . $annee2."-".$mois2."-01'";
		return $champDate . ">='" . $annee1 . "-" . $mois1 . "-01' AND " . $champDate . "<'" . $annee2 . "-" . $mois2 . "-01'";
	}

	public static function formatDateDB($date) {
		$newDate = explode('/',$date);
		return $newDate[2].$newDate[1].$newDate[0];
	}

	public static function formatDateDB2($date) {
		$newDate = explode('-',$date);
		return $newDate[2].$newDate[1].$newDate[0];
	}

	public static function formatDateCTV($date, $lng) {
		$newDate = explode('/',(string)$date);
		if ($lng == 'en') {
			return (string)$newDate[2]."-".(string)$newDate[0]."-".(string)$newDate[1];
		}
		else
		{
			return (string)$newDate[2]."-".(string)$newDate[1]."-".(string)$newDate[0];
		}
	}

	public static function premierJour($jour) {
		if ($jour==1) {
			return "1<sup>er</sup>";
		}
		else
		{
			return $jour;
		}
	}

	public static function premierJour2($jour) {
		if ($jour==1) {
			return "1er";
		}
		else
		{
			return $jour;
		}
	}

	public static function nomMois($mois) {
		$str="";
		if($mois == 1) $str = "janvier";
		if($mois == 2) $str = "f&eacute;vrier";
		if($mois == 3) $str = "mars";
		if($mois == 4) $str = "avril";
		if($mois == 5) $str = "mai";
		if($mois == 6) $str = "juin";
		if($mois == 7) $str = "juillet";
		if($mois == 8) $str = "ao&ucirc;t";
		if($mois == 9) $str = "septembre";
		if($mois == 10) $str = "octobre";
		if($mois == 11) $str = "novembre";
		if($mois == 12) $str = "d&eacute;cembre";
		return $str;
	}

	public static function nomMois2($mois) {
		$str="";
		if($mois == 1) $str = "janvier";
		if($mois == 2) $str = "février";
		if($mois == 3) $str = "mars";
		if($mois == 4) $str = "avril";
		if($mois == 5) $str = "mai";
		if($mois == 6) $str = "juin";
		if($mois == 7) $str = "juillet";
		if($mois == 8) $str = "août";
		if($mois == 9) $str = "septembre";
		if($mois == 10) $str = "octobre";
		if($mois == 11) $str = "novembre";
		if($mois == 12) $str = "décembre";
		return $str;
	}

	public static function nomJourSemaine($num) {
		$str="";
		if($num == 1) $str = "Lundi";
		if($num == 2) $str = "Mardi";
		if($num == 3) $str = "Mercredi";
		if($num == 4) $str = "Jeudi";
		if($num == 5) $str = "Vendredi";
		if($num == 6) $str = "Samedi";
		if($num == 7) $str = "Dimanche";
		return $str;
	}

}
