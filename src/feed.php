<?php
/*
 * Copyright (c) 2020-2020. 28/10/2020 19:04. Johann Frot - B4K
 */

namespace b4k\phpTools;

class feed
{

	public static function sitemap($list) {

		$xml = '<?xml version="1.0" encoding="UTF-8"?>';

		if (isset($list) && count($list) > 0)
		{
			$xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

			foreach ($list as $url) {
				$xml .= '<url><loc>' . trim($url) . '</loc></url>';
			}

			$xml .= '</urlset>';
		}

		return $xml;
	}

	public static function rss($feedItems, feedHeader $feedHeader) {

		$xml = '<?xml version="1.0" encoding="utf-8"?>';

		if (isset($feedItems) && count($feedItems) > 0)
		{
			$xml .= '<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">';
			$xml .= '<channel>';
			$xml .= '<title>' . $feedHeader->title . '</title>';
			$xml .= '<link>' . $feedHeader->link . '</link>';
			$xml .= '<description>' . $feedHeader->description . '</description>';
			$xml .= '<language>' . $feedHeader->language . '</language>';
			$xml .= '<atom:link href="' . $feedHeader->atomLink . '" rel="self" type="application/rss+xml" />';
//				$xml .= '<icon>' . $feedHeader->faviconLink . '</icon>';

			foreach ($feedItems as $feedItem) {
				$xml .= '<item>';
				$xml .= '<title><![CDATA[' . $feedItem->title . ']]></title>';
				$xml .= '<link>' . $feedItem->link . '</link>';
				$xml .= '<description><![CDATA[' . $feedItem->description . ']]></description>';
				$xml .= '<guid isPermaLink="false">' . $feedItem->guid . '</guid>';

				if(isset($feedItem->pubDate) && $feedItem->pubDate != '')
				{
					$xml .= '<pubDate>' . date('r', strtotime($feedItem->pubDate)) . '</pubDate>';
				}

				if(isset($feedItem->image_url) && $feedItem->image_url != ''
				&& isset($feedItem->image_type) && $feedItem->image_type != ''
				&& isset($feedItem->image_length) && $feedItem->image_length != '')
				{
					$xml .= '<enclosure url="' . $feedItem->image_url . '" type="' . $feedItem->image_type . '" length="' . $feedItem->image_length . '"/>';
				}

				$xml .= '</item>';
			}

			$xml .= '</channel>';
			$xml .= '</rss>';
		}

		return $xml;
	}
}
